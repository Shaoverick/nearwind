package com.sidewind.nearwind;

import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.widget.TextView;

import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

public class MainActivity extends AppCompatActivity implements SensorEventListener {

    private TextView textView;
    private TextView textView2;
    private TextView textView3;
    private TextView textView4;
    private TextView textView5;

    private int evenCounter = 0;
    private int oddCounter = 0;
    private int distance;

    private LineChart lineChart;
    private LineDataSet lineDataSet;

    private List<Entry> entryList = new ArrayList<>();

    private Timer timer = new Timer();
    private int counterTime = 0;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        textView = findViewById(R.id.textview);
        textView2 = findViewById(R.id.textview2);
        textView3 = findViewById(R.id.textview3);
        textView4 = findViewById(R.id.textview4);
        textView5 = findViewById(R.id.textview5);
        lineChart = findViewById(R.id.lineChart);

        SensorManager sensorManager = (SensorManager) getSystemService(SENSOR_SERVICE);
        Sensor sensor = sensorManager.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD);
        sensorManager.registerListener(this, sensor, SensorManager.SENSOR_DELAY_NORMAL);

        timer.schedule (minuteTask, 0L, 1000);
    }

    @Override
    public void onSensorChanged(SensorEvent event) {
        textView.setText(String.valueOf(calcVectorModule(event)));
        process(calcVectorModule(event));
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) { }

    private double calcVectorModule(SensorEvent event) {
        return Math.sqrt(event.values[0] * event.values[0] + event.values[1] * event.values[1] + event.values[2] * event.values[2]);
    }

    @SuppressLint("SetTextI18n")
    private void process(double value) {
        value = Math.round(value * 10000);

        boolean isodd = isOdd((int) value);

        if(isodd) {
            oddCounter++;
            textView2.setText("" + oddCounter);
        } else {
            evenCounter++;
            textView3.setText("" + evenCounter);
        }

        distance = evenCounter - oddCounter;
        textView4.setText("" + distance);
    }

    boolean isOdd(int val) {
        return (val & 0x01) != 0;
    }

    private TimerTask minuteTask = new TimerTask() {
        @Override
        public void run () {
            Entry entry = new Entry();
            entry.setX(counterTime);
            entry.setY(distance);

            entryList.add(entry);
            lineDataSet = new LineDataSet (entryList, "Datos");
            LineData data = new LineData(lineDataSet);
            lineChart.setData(data);
            lineChart.getXAxis().setAxisMaximum(counterTime);

            XAxis xAxis = lineChart.getXAxis();
            xAxis.setGranularity(1f);

            lineChart.invalidate();

            counterTime++;
        }
    };

}
